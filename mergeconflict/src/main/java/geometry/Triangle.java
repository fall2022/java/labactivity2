package geometry;

public class Triangle {
    private double base;
    private double height;

    public Triangle(double base, double height){
        this.base = base;
        this.height = height;
    }

    public double getBase() {
        return this.base;
    }

    public double getHeight() {
        return this.height;
    }

    public double getArea(){
        return base * height / 2;
    }

    public String toString(){
        return "This is a triangle with a base of "+ this.getBase() +" and a height of "+ this.getHeight();
    }
}
