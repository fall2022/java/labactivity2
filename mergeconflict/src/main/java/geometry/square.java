package geometry;

public class square{
    
    private double length;

    public square(double l) {
        this.length= l;
    }

    public double getLength() {
        return this.length;
    }

    public double getArea() {
        return this.length*this.length;
    }

    public String toString(){
        return "The length is: " + this.length +" and the area is: " + this.getArea() ;

    }



}
