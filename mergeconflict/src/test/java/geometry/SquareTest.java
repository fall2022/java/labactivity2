package geometry;

import static org.junit.Assert.*;
import org.junit.Test;

public class SquareTest {
    final double TOLERANCE = 0.0000001 ;

    @Test
    public void testGetLength(){

        square S = new square(3);
        assertEquals(3, S.getLength(), TOLERANCE);
    }

    @Test
    public void testGetArea(){

        square S = new square(9);
        assertEquals(81, S.getArea(), TOLERANCE);
    }

    @Test
    public void testToString(){

        square S = new square(8);
        String expected = "The length is: " + S.getLength() +" and the area is: " + S.getArea();
        assertEquals(expected, S.toString());
    }
}
